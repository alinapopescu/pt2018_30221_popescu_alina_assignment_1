
public class Operatii
{
	public static   Polinom adunare(Polinom a,Polinom b)
	{
	       Polinom rezultat=new Polinom();
	       int gradA=a.gradPolinom();
	       int gradB=b.gradPolinom();
	       if(gradA>gradB)
	       {
	           for(int i=0;i<=gradA;i++)
	           {
	               if(i<=gradB)
	                   rezultat.addCoeficient(a.coeficient(i)+b.coeficient(i));
	               else
	                   rezultat.addCoeficient(a.coeficient(i));
	           }
	       }
	        else
	        {
	           for(int i=0;i<=gradB;i++)
	           {
	               if(i<=gradA)
	                   rezultat.addCoeficient(a.coeficient(i)+b.coeficient(i));
	               else
	                   rezultat.addCoeficient(b.coeficient(i));
	           }
	       }
	    return rezultat;
	    }
	     public static Polinom scadere(Polinom a,Polinom b)
	     {
	      Polinom rezultat=new Polinom();
	        int gradA=a.gradPolinom();
	        int gradB=b.gradPolinom();
	        if(gradA>gradB)
	        {
	            for(int i=0;i<=gradA;i++)
	            {
	                if(i<=gradB)
	                    rezultat.addCoeficient(a.coeficient(i)-b.coeficient(i));
	                else
	                    rezultat.addCoeficient(a.coeficient(i));
	            }
	        }
	        else
	        {
	            for(int i=0;i<=gradB;i++)
	            {
	                if(i<=gradA)
	                    rezultat.addCoeficient(a.coeficient(i)-b.coeficient(i));
	                else
	                    rezultat.addCoeficient(-b.coeficient(i));
	            }
	        }
	        return rezultat;
	    }
	   public static  Polinom inmultire(Polinom a,Polinom b)
	   {
	        Polinom rezultat=new Polinom();
	        int gradA=a.gradPolinom();
	        int gradB=b.gradPolinom();
	        for (int i = 0; i <= gradA; i++)
	            for (int j = 0; j <= gradB; j++)
	          {
	              if (i + j <= rezultat.gradPolinom()) 
	              {
	                  rezultat.inmulteste(i + j, rezultat.coeficient(i + j) + (a.coeficient(i) * b.coeficient(j)));
	              } 
	              else 
	              {
	                  rezultat.addCoeficient(a.coeficient(i) * b.coeficient(j));
	              }
	          }
	      return rezultat;
	  }
	     public static Polinom derivare(Polinom a)
	     {
	        Polinom rezultat=new Polinom();
	        int gradA=a.gradPolinom();
	        for(int i=1;i<=gradA;i++)
	        {
	            rezultat.addCoeficient(a.coeficient(i)*i);
	        }
	    return rezultat;
	    }
	     /*public Polinom integrare (Polinom a)
	     {
	    	 Polinom rezultat=new Polinom();
	     }*/
	    	 
	
}

