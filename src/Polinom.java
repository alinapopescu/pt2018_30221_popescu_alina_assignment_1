import java.util.ArrayList;
import java.util.List;
public class Polinom {

	    private List<Integer> lista;
	    Polinom(){
	        lista=new ArrayList<>();
	    }
	    void addCoeficient(Integer x){
	        lista.add(x);
	    }
	    int gradPolinom(){
	      return lista.size() - 1;
	    }
	    int coeficient(int poz){ 
	        return lista.get(poz);
	    }
	   void inmulteste(int x, Integer y) {
	        lista.set(x, y);
	    }
	  
	    @Override
		public String toString() {
			if ( gradPolinom() == 0) {
				return "" +  coeficient(0);
			}
			if (( gradPolinom() == 1) && ( coeficient(1) == 1)) {
				return "x + " +  coeficient(0);
			} else if (( gradPolinom() == 1) && ( coeficient(1) == -1)) {
				return "-x + " +  coeficient(0);
			} else if ( gradPolinom() == 1) {
				return  coeficient(1) + "x + " +  coeficient(0);
			}

			String s =  coeficient( gradPolinom()) + "x^" +  gradPolinom();
			for (int i =  gradPolinom() - 1; i >= 0; i--) {
				if ( coeficient(i) == 0) {
					continue;
				} else if (( coeficient(i) > 0)) {
					s = s + " + " + ( coeficient(i));

				} else if (( coeficient(i) < 0)) {
					s = s + " - " + (- coeficient(i));
				}
				if (i == 1) {
					s = s + "x";
				} else if (i > 1) {
					s = s + "x^" + i;
				}
			}
			return s;
		}
	
	   
}
