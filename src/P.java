
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class P {
    private Interfata view;
    private Polinom a;
    private Polinom b;
    public P (Interfata view){//constructor
        this.view=view;
        this.view.addAdListener(new AdunareListener());
        this.view.addScadListener(new ScadereListener());
        this.view.addInmultListener(new InmultireListener());
        this.view.addDerivListener(new DerivareListener());
        this.view.addClearListener(new ClearListener());
        this.view.addAdaugaAListener(new AdaugaAListener());
        this.view.addAdaugaBListener(new AdaugaBListener());
    }

    class AdunareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
           Polinom rezultat=new Polinom();
           rezultat=Operatii.adunare(a, b);
           view.setText3(rezultat.toString());
        }
    }
    class ScadereListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	  Polinom rezultat=new Polinom();
	           rezultat=Operatii.scadere(a, b);
	           view.setText4(rezultat.toString());
        }
    }

    class InmultireListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	  Polinom rezultat=new Polinom();
	           rezultat=Operatii.inmultire(a, b);
	           view.setText5(rezultat.toString());
        }
    }

    class DerivareListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	  Polinom rezultat=new Polinom();
	           rezultat=Operatii.derivare(a);
	           view.setText6(rezultat.toString());
        }
    }

    class ClearListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	view.setText1("");
        	view.setText2("");
        	view.setText3("");
    }
}
    class AdaugaAListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	a=new Polinom();
        		 String coef=view.getTheCoef();
        		 String[] coef2= coef.split(" ");
        		 for(int i=0;i<coef2.length;i++){
                 a.addCoeficient(Integer.parseInt(coef2[i]));
        		 }
                 view.setText1(a.toString());
   
    }
}
    class AdaugaBListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	b=new Polinom();
   		 String coef=view.getTheCoef();
   		 String[] coef2= coef.split(" ");
   		 for(int i=0;i<coef2.length;i++){
            b.addCoeficient(Integer.parseInt(coef2[i]));
            }
            view.setText2(b.toString());
    }
}

}
